package de.fewycoding.rezepteapp.domain.model

data class Rezept (
        val id: Int?,
        val title: String?,
        val publisher: String?,
        val image: String?,
        val rating: Int?,
        val sourceUrl: String?,
        val beschreibung: String?,
        val kochAnweisung: String?,
        val zutaten: List<String> = listOf(),
        val datumHinzugefuegt: String?,
        val datumUpdated: String?
)