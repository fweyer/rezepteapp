package de.fewycoding.rezepteapp.dependencyinjection

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import de.fewycoding.rezepteapp.mapper.RezeptModelMapper
import de.fewycoding.rezepteapp.network.RezepteRestWebService
import de.fewycoding.rezepteapp.repository.RezeptRepository
import de.fewycoding.rezepteapp.repository.RezeptRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideRezeptRepository(rezepteRestWebService: RezepteRestWebService,
                                rezeptModelMapper: RezeptModelMapper): RezeptRepository {
        return RezeptRepositoryImpl(rezepteRestWebService,rezeptModelMapper)
    }

}