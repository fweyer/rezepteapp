package de.fewycoding.rezepteapp.repository

import de.fewycoding.rezepteapp.domain.model.Rezept
import de.fewycoding.rezepteapp.mapper.RezeptModelMapper
import de.fewycoding.rezepteapp.network.RezepteRestWebService

class RezeptRepositoryImpl(
    private val rezepteRestWebService: RezepteRestWebService,
    private val mapper: RezeptModelMapper
) : RezeptRepository {

    override suspend fun search(token: String, page: Int, query: String): List<Rezept> {
        val rezepteListResponse = rezepteRestWebService.search(token, page, query)
        return mapper.fromRezeptResponsetoRezeptList(rezepteListResponse.rezepte)
    }

    override suspend fun findRezeptByID(token: String, id: Int): Rezept {
        val rezeptResponse = rezepteRestWebService.findRezeptByID(token, id)
        return mapper.mapToModel(rezeptResponse)
    }


}