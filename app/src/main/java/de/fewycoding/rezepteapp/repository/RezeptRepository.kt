package de.fewycoding.rezepteapp.repository

import de.fewycoding.rezepteapp.domain.model.Rezept

interface RezeptRepository  {

    suspend fun search(token: String, page: Int, query: String): List<Rezept>

    suspend fun findRezeptByID(token: String, id: Int): Rezept

}