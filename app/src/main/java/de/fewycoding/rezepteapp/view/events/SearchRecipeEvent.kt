package de.fewycoding.rezepteapp.view.events

 sealed class SearchRecipeEvent {

     object NewSearch: SearchRecipeEvent()
     object NextPage: SearchRecipeEvent()
}