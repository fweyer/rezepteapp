package de.fewycoding.rezepteapp.view.viewmodel

import android.content.ContentValues.TAG
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.fewycoding.rezepteapp.domain.model.Rezept
import de.fewycoding.rezepteapp.repository.RezeptRepository
import de.fewycoding.rezepteapp.view.events.SearchRecipeEvent
import de.fewycoding.rezepteapp.view.konstanten.FoodKategorie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

const val PAGE_SIZE = 30

@HiltViewModel
class RezeptListViewModel
@Inject constructor(

    private val rezeptRepository: RezeptRepository,

    @Named("token")
    private val token: String

): ViewModel(){

    val rezepte : MutableState<List<Rezept>> = mutableStateOf(ArrayList())
    val abfrageText : MutableState<String> = mutableStateOf("")
    val selectedKategorie : MutableState<FoodKategorie?> = mutableStateOf(null)
    var horizontalScrollposition:Float = 0f
    var loading :MutableState<Boolean> = mutableStateOf(false)
    val page = mutableStateOf(1)
    var recipeListScrollPosition = 0


    fun onSearchTrigger(event: SearchRecipeEvent){
        viewModelScope.launch(Dispatchers.IO) {
            when(event){
               is SearchRecipeEvent.NewSearch -> { neueSuche() }
               is SearchRecipeEvent.NextPage -> { nextPage() }
            }
        }
    }

    private suspend fun neueSuche(){
            loading.value = true
            resetSucheStatus()
            delay(1000)
            val result = rezeptRepository.search(
                token = token,
                page = page.value,
                query = abfrageText.value
            )
            rezepte.value = result;
            loading.value = false
    }

    private suspend fun nextPage(){
            // If statement verhindert das next page nicht viele male in einer millisikunde aufgerufen wird
        if ((recipeListScrollPosition + 1) >= (page.value * PAGE_SIZE)){
            loading.value = true;
            incrementPage()
            delay(1000)
            if (page.value > 1) {
                val result = rezeptRepository.search(
                    token = token,
                    page = page.value,
                    query = abfrageText.value
                )
                Log.d(TAG, "nextPage: ${result}")
                appendRecipes(result)
                loading.value = false;
            }
        }
    }


    private fun incrementPage(){
        page.value = page.value + 1
    }

    private fun appendRecipes( rezepte: List<Rezept>){
        val current = ArrayList(this.rezepte.value)
        current.addAll(rezepte)
        this.rezepte.value = current
    }

    fun onChangeRecipeScrollPosition(position: Int){
        recipeListScrollPosition = position
    }


    fun onAbfrageTextChange(text: String){
        abfrageText.value = text
    }

    fun onSelectedKategorieChange(newKategorie:String){
        selectedKategorie.value = FoodKategorie.getFoodKategorie(newKategorie)
    }

    fun setHorizontalScrollPosition(newHorizontalScrollPosition: Float){
        horizontalScrollposition = newHorizontalScrollPosition
    }

    private fun resetSucheStatus(){
        rezepte.value = listOf()
        page.value = 1
        onChangeRecipeScrollPosition(0)
        if (selectedKategorie.value?.value != abfrageText.value)
            clearSelectedKategorie()
    }

    private fun clearSelectedKategorie(){
        selectedKategorie.value = null
    }

}