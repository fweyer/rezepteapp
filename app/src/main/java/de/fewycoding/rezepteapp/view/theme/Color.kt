package de.fewycoding.rezepteapp.view.theme

import androidx.compose.ui.graphics.Color

val GREEN400 = Color(0xCC9ACC32)
val GREEN600 = Color(0xCC6B8E23)
val Blue700 = Color(0xFF1976D2)

val Teal300 = Color(0xFF1AC6FF)

val Grey1 = Color(0xFFF2F2F2)

val Black1 = Color(0xFF222222)
val Black2 = Color(0xFF000000)

val RedErrorDark = Color(0xFFB00020)
val RedErrorLight = Color(0xFFEF5350)


