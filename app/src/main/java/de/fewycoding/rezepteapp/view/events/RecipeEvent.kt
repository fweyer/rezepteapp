package de.fewycoding.rezepteapp.view.events

sealed class RecipeEvent {

    data class GetRecipeEvent(
        val id: Int?
    ): RecipeEvent()

}