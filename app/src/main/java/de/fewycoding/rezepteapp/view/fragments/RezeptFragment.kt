package de.fewycoding.rezepteapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import de.fewycoding.rezepteapp.domain.model.Rezept
import de.fewycoding.rezepteapp.view.CircularProgressbar
import de.fewycoding.rezepteapp.view.components.RezeptView
import de.fewycoding.rezepteapp.view.events.RecipeEvent
import de.fewycoding.rezepteapp.view.viewmodel.RezeptViewModel

@AndroidEntryPoint
class RezeptFragment : Fragment() {

    private val viewModel: RezeptViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.getInt("rezeptID").let { recipeID ->
            viewModel.onTriggerEvent(RecipeEvent.GetRecipeEvent(recipeID))
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(color = MaterialTheme.colors.background)
                ) {
                    RezeptDetailScreen(rezept = viewModel.rezept.value)
                    CircularProgressbar(isDisplay = viewModel.loading.value)
                }
            }
        }
    }

    @Composable
    fun RezeptDetailScreen(rezept: Rezept?){
        Column(modifier = Modifier.padding(16.dp)) {
            if (rezept != null) {
              RezeptView(rezept = rezept)
            }
        }
    }
    
}

