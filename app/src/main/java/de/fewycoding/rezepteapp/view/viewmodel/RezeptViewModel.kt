package de.fewycoding.rezepteapp.view.viewmodel

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.fewycoding.rezepteapp.domain.model.Rezept
import de.fewycoding.rezepteapp.repository.RezeptRepository
import de.fewycoding.rezepteapp.view.events.RecipeEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class RezeptViewModel
@Inject constructor(

    private val rezeptRepository: RezeptRepository,

    @Named("token")
    private val token: String

): ViewModel(){

    val rezept: MutableState<Rezept?> = mutableStateOf(null)
    val loading = mutableStateOf(false)


    fun onTriggerEvent(event: RecipeEvent){
        viewModelScope.launch(Dispatchers.IO) {
            when(event){
                is RecipeEvent.GetRecipeEvent -> { findRecipeByID(event.id) }
            }
        }
    }

    private suspend fun findRecipeByID(id: Int?) {
        if (id == null)
            return

        loading.value = true
        delay(1000)
        val result = rezeptRepository.findRezeptByID(
            token = token,
            id = id
        )
        rezept.value = result;
        loading.value = false
    }


}