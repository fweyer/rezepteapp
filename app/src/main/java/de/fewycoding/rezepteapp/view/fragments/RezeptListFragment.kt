package de.fewycoding.rezepteapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import de.fewycoding.rezepteapp.view.CircularProgressbar
import de.fewycoding.rezepteapp.view.components.ListViewRezepte
import de.fewycoding.rezepteapp.view.components.SucheAppbar
import de.fewycoding.rezepteapp.view.events.SearchRecipeEvent
import de.fewycoding.rezepteapp.view.theme.AppTheme
import de.fewycoding.rezepteapp.view.theme.ThemeState
import de.fewycoding.rezepteapp.view.viewmodel.RezeptListViewModel

@OptIn(ExperimentalMaterialApi::class)
@AndroidEntryPoint
class RezeptListFragment: Fragment() {

    val viewModel: RezeptListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onSearchTrigger(SearchRecipeEvent.NewSearch)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                AppTheme(lightTheme = ThemeState.isLight.value,) {
                Scaffold(
                    topBar = { TopbarRezeptListFragment() }
                ) {
                    BodyRezeptListFragment()
                }
                }
            }
        }
    }

    @Composable
    fun TopbarRezeptListFragment(){
        SucheAppbar(
            textInput = viewModel.abfrageText.value,
            onTextChange = { viewModel.onAbfrageTextChange(it) },
            onSelectedKategorie = viewModel.selectedKategorie.value,
            onSucheAusfuehren = { viewModel.onSearchTrigger(SearchRecipeEvent.NewSearch) },
            onSelectKategorieChange = { viewModel.onSelectedKategorieChange(it) },
            getScrollPosition = { viewModel.horizontalScrollposition },
            setScrollPosition = { viewModel.setHorizontalScrollPosition(it) },
            onToggleTheme = { ThemeState.toogleTheme() }
        )
    }


    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun BodyRezeptListFragment(){


        Column(
            modifier = Modifier.padding(8.dp)
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = MaterialTheme.colors.background)
            ) {
                ListViewRezepte(
                    rezepte = viewModel.rezepte.value,
                    page = viewModel.page.value,
                    loading = viewModel.loading.value,
                    onChangeRecipeScrollPosition = { viewModel.onChangeRecipeScrollPosition(it)},
                    nextPage = { viewModel.onSearchTrigger(SearchRecipeEvent.NextPage) },
                    colums = 2,
                    navController = findNavController()
                )
                CircularProgressbar(isDisplay = viewModel.loading.value)
            }
        }
    }
}