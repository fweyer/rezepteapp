package de.fewycoding.rezepteapp.view.konstanten

enum class FoodKategorie(val value: String) {
    CHICKEN("Chicken"),
    BEEF("Beef"),
    SOUP("Soup"),
    DESSERT("Dessert"),
    VEGETARIAN("Vegetarian"),
    MILK("Milk"),
    VEGAN("Vegan"),
    PIZZA("Pizza"),
    DONUT("Donut");

    companion object{
        fun getFoodKategorie(value: String): FoodKategorie? {
            val map = values().associateBy(FoodKategorie::value)
            return map[value]
        }

    }
}
