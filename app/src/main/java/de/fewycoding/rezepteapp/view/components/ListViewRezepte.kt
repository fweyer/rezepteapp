package de.fewycoding.rezepteapp.view.components

import android.os.Bundle
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import de.fewycoding.rezepteapp.R
import de.fewycoding.rezepteapp.domain.model.Rezept
import de.fewycoding.rezepteapp.view.viewmodel.PAGE_SIZE

@ExperimentalFoundationApi
@Composable
fun ListViewRezepte(
    rezepte: List<Rezept>,
    page: Int,
    loading: Boolean,
    onChangeRecipeScrollPosition: (Int) -> Unit,
    nextPage: () -> Unit,
    colums: Int,
    navController: NavController
){
    LazyVerticalGrid(
        cells = GridCells.Fixed(colums),
    ) {
        itemsIndexed(items = rezepte) { index, item ->
            onChangeRecipeScrollPosition(index)
            if ((index + 1) >= (page * PAGE_SIZE) && !loading){
                nextPage()
            }
            RezeptCard(rezept = item, onClick = { onClickRezeptCard(item, navController)})
        }
    }
}

fun onClickRezeptCard(item: Rezept, navController: NavController){
    if (item.id != null){
        val bundle = Bundle()
        bundle.putInt("rezeptID", item.id)
        navController.navigate(R.id.rezeptFragment, bundle)
    }
}

