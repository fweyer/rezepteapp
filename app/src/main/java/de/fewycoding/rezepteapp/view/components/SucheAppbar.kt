package de.fewycoding.rezepteapp.view.components

import androidx.compose.foundation.ScrollableRow
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import de.fewycoding.rezepteapp.view.konstanten.FoodKategorie

@Composable
fun SucheAppbar(textInput: String,
                onTextChange: (String) -> Unit,
                onSucheAusfuehren: () -> Unit,
                onSelectedKategorie: FoodKategorie?,
                onSelectKategorieChange: (String) -> Unit,
                getScrollPosition: () -> Float,
                setScrollPosition: (Float) -> Unit,
                onToggleTheme: () -> Unit
)
{

    Surface(
            modifier = Modifier.fillMaxWidth(),
            color = MaterialTheme.colors.surface,
            elevation = 8.dp
    )
    {
        Column {
            Row(modifier = Modifier.fillMaxWidth(),
            )
            {
                TextField(
                    modifier = Modifier
                        .fillMaxWidth(0.90f)
                        .padding(8.dp),
                    value = textInput,
                    backgroundColor = MaterialTheme.colors.surface,
                    onValueChange = { text -> onTextChange(text)},
                    label = {
                        Text(text = "Suche")
                    },
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Text,
                        imeAction = ImeAction.Search
                    ),
                    leadingIcon = {
                        Icon(Icons.Filled.Search)
                    },
                    onImeActionPerformed = { action, softKeyboardController ->
                        if (action == ImeAction.Search)
                            onSucheAusfuehren()
                            softKeyboardController?.hideSoftwareKeyboard()
                    },
                    textStyle = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        background = MaterialTheme.colors.surface
                    )
                )
                IconButton(
                    modifier = Modifier.align(Alignment.CenterVertically).padding(8.dp),
                    onClick = onToggleTheme
                ) {
                    Icon(Icons.Filled.MoreVert)
                }
            }
            val scrollState = rememberScrollState()
            ScrollableRow(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
                scrollState = scrollState
            )
            {
                scrollState.scrollTo(getScrollPosition())
                for (kategorie in FoodKategorie.values()){
                    KategorieChip(
                        kategorie = kategorie.value,
                        isSelected = kategorie == onSelectedKategorie,
                        onSelectKategorieChange = { kategorie ->
                            onSelectKategorieChange(kategorie)
                            setScrollPosition(scrollState.value)
                        },
                        onSucheAusfuehrenMitKategorie = { text ->
                            onSucheAusfuehren()
                            onTextChange(text)
                        }
                    )
                }
            }
        }
    }
}


