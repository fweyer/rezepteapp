package de.fewycoding.rezepteapp.view.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import de.fewycoding.rezepteapp.R
import de.fewycoding.rezepteapp.domain.model.Rezept
import de.fewycoding.rezepteapp.view.util.ladeImageBild

@Composable
fun RezeptView(rezept: Rezept){
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            item {
                val image = ladeImageBild(url = rezept.image.toString(), defaultImage = R.drawable.empty_plate).value
                image?.let { img ->
                    Image(
                        bitmap = img.asImageBitmap(),
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(260.dp)
                        ,
                        contentScale = ContentScale.Crop,
                    )
                }
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 4.dp)
                    ){
                        Text(
                            text = rezept.title ?: "",
                            modifier = Modifier
                                .fillMaxWidth(0.85f)
                                .wrapContentWidth(Alignment.Start)
                                .padding(top = 12.dp, bottom = 12.dp)
                            ,
                            style = MaterialTheme.typography.h5
                        )
                        val rank = rezept.rating.toString()
                        Text(
                            text = rank,
                            modifier = Modifier
                                .fillMaxWidth()
                                .wrapContentWidth(Alignment.End)
                                .align(Alignment.CenterVertically)
                            ,
                            style = MaterialTheme.typography.h5
                        )
                    }
                    for(zutat in rezept.zutaten){
                        Text(
                            text = zutat,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(bottom = 4.dp)
                            ,
                            style = MaterialTheme.typography.body1
                        )
                    }
                    Text(
                        text = rezept.kochAnweisung ?: "",
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 4.dp)
                        ,
                        style = MaterialTheme.typography.body1
                    )
                }
            }
        }
}