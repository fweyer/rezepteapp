package de.fewycoding.rezepteapp.view.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import de.fewycoding.rezepteapp.R
import de.fewycoding.rezepteapp.domain.model.Rezept
import de.fewycoding.rezepteapp.view.util.ladeImageBild

@Composable
fun RezeptCard(rezept:Rezept, onClick:() -> Unit){
    Card(
        shape = MaterialTheme.shapes.small,
        modifier = Modifier
            .padding(bottom = 6.dp, top = 6.dp, start = 6.dp, end = 6.dp )
            .fillMaxWidth()
            .clickable(onClick = onClick),
        elevation = 8.dp
    ){
        Column{
            rezept.image?.let { url ->
                val bild = ladeImageBild(url = url, defaultImage = R.drawable.empty_plate).value
                bild?.let {
                    Image(
                        bitmap = bild.asImageBitmap(),
                        modifier = Modifier
                            .fillMaxWidth()
                            .preferredHeight(225.dp),
                        contentScale = ContentScale.Crop
                    )
                }
            }
            rezept.let {
                Row (
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 8.dp, bottom = 8.dp, start = 6.dp, end = 6.dp)
                        .preferredHeight(60.dp),
                ){
                    Text(
                        text = rezept.title ?: "Kein Titel vorhanden",
                        modifier = Modifier
                            .fillMaxWidth(0.85f)
                            .wrapContentWidth(Alignment.Start),
                        style = MaterialTheme.typography.body1

                    )
                    Text(
                        text = rezept.rating.toString(),
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentWidth(Alignment.End)
                            .align(Alignment.CenterVertically),
                        style = MaterialTheme.typography.body1
                    )
                }
            }
        }
    }
}

