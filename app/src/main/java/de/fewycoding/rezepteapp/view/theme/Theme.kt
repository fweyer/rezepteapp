package de.fewycoding.rezepteapp.view.theme

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color


private val LightThemeColors = lightColors(
    primary = GREEN600,
    primaryVariant = GREEN400,
    onPrimary = Black2,
    secondary = Color.White,
    secondaryVariant = Teal300,
    onSecondary = Color.Black,
    error = RedErrorDark,
    onError = RedErrorLight,
    background = Grey1,
    onBackground = Color.Black,
    surface = Color.White,
    onSurface = Black2,
)

private val DarkThemeColors = darkColors(
    primary = Blue700,
    primaryVariant = Color.White,
    onPrimary = Color.White,
    secondary = Black1,
    onSecondary = Color.White,
    error = RedErrorLight,
    background = Color.Black,
    onBackground = Color.Black,
    surface = Black1,
    onSurface = Color.White,
)

@ExperimentalMaterialApi
@Composable
fun AppTheme(
    lightTheme: Boolean,
    content: @Composable () -> Unit,
) {
    MaterialTheme(
        colors = if (lightTheme) LightThemeColors else DarkThemeColors,
    ){
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(color = if(lightTheme) Grey1 else Color.Black)
        ){
            content()
        }
    }
}





























