package de.fewycoding.rezepteapp.view.theme

import androidx.compose.runtime.mutableStateOf


object ThemeState {

    var isLight = mutableStateOf(true)

    fun toogleTheme(){
        isLight.value = !isLight.value
    }
}