package de.fewycoding.rezepteapp.mapper

interface ModelMapper<Input, Output> {

    fun mapToModel(model:Input):Output

    fun mapFromModel(model:Output):Input

}