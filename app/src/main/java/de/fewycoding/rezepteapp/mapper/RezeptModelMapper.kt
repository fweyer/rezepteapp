package de.fewycoding.rezepteapp.mapper

import de.fewycoding.rezepteapp.domain.model.Rezept
import de.fewycoding.rezepteapp.network.responses.RezeptResponse

class RezeptModelMapper : ModelMapper<RezeptResponse, Rezept> {

    override fun mapToModel(model: RezeptResponse): Rezept {
        return Rezept(
            id = model.pk,
            title = model.title,
            publisher = model.publisher,
            image = model.featured_image,
            rating = model.rating,
            sourceUrl = model.sourceUrl,
            beschreibung = model.description,
            kochAnweisung = model.cooking_instructions,
            zutaten = model.ingredients ?: listOf(),
            datumHinzugefuegt = model.date_added,
            datumUpdated = model.date_updated
        )
    }

    override fun mapFromModel(model: Rezept): RezeptResponse {
        return RezeptResponse(
            pk = model.id,
            title = model.title,
            publisher = model.publisher,
            featured_image = model.image,
            rating = model.rating,
            sourceUrl = model.sourceUrl,
            description = model.beschreibung,
            cooking_instructions = model.kochAnweisung,
            ingredients = model.zutaten,
            date_added = model.datumHinzugefuegt,
            date_updated = model.datumUpdated
        )
    }

    fun fromRezeptResponsetoRezeptList(intial: List<RezeptResponse>): List<Rezept>{
        return intial.map { mapToModel(it) }
    }

    fun fromRezeptListtoRezeptResponse(intial: List<Rezept>): List<RezeptResponse>{
        return intial.map { mapFromModel(it) }
    }

}