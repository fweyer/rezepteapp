package de.fewycoding.rezepteapp.network.responses

import com.google.gson.annotations.SerializedName

data class RezeptResponse (

    @SerializedName("pk")
    val pk: Int?,

    @SerializedName("title")
    val title: String?,

    @SerializedName("publisher")
    val publisher: String?,

    @SerializedName("featured_image")
    val featured_image: String?,

    @SerializedName("rating")
    val rating: Int?,

    @SerializedName("source_url")
    val sourceUrl: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("cooking_instructions")
    val cooking_instructions: String?,

    @SerializedName("ingredients")
    val ingredients: List<String>?,

    @SerializedName("date_added")
    val date_added: String?,

    @SerializedName("date_updated")
    val date_updated: String?
)