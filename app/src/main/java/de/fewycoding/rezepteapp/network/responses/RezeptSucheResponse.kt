package de.fewycoding.rezepteapp.network.responses

import com.google.gson.annotations.SerializedName

data class RezeptSucheResponse (

        @SerializedName("count")
        var count: Int,

        @SerializedName("results")
        var rezepte: List<RezeptResponse>
)