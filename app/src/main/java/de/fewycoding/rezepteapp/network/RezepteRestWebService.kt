package de.fewycoding.rezepteapp.network

import de.fewycoding.rezepteapp.network.responses.RezeptResponse
import de.fewycoding.rezepteapp.network.responses.RezeptSucheResponse
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface RezepteRestWebService {

    @GET("search")
    suspend fun search(
        @Header("Authorization") token: String,
        @Query("page") page: Int,
        @Query("query") query: String
    ): RezeptSucheResponse

    @GET("get")
    suspend fun findRezeptByID(
        @Header("Authorization") token: String,
        @Query("id") id: Int,
    ): RezeptResponse

}